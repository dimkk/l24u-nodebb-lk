<div class="row">
	<div class="col-sm-2 col-xs-12 settings-header">Управление параметрами личного кабинета (В РАЗРАБОТКЕ)</div>
	<div class="col-sm-10 col-xs-12">
		

		<div class="form-group">
			<label>[[admin/settings/chat:max-length]]</label>
			<input type="text" class="form-control" value="1000" data-field="maximumChatMessageLength">
		</div>

		<div class="form-group">
			<label>[[admin/settings/chat:max-room-size]]</label>
			<input type="text" class="form-control" value="0" data-field="maximumUsersInChatRoom">
		</div>


		<div class="form-group">
			<label>[[admin/settings/chat:delay]]</label>
			<input type="text" class="form-control" value="200" data-field="chatMessageDelay">
		</div>
	</div>
</div>

</div>

<button id="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
	<i class="material-icons">save</i>
</button>

<script>
	require(['admin/settings'], function(Settings) {
		Settings.prepare();
		Settings.populateTOC();
	});
</script>
