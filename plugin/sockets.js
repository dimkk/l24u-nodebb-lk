(function(Sockets) {
    'use strict';

    var constants = require('./constants'),
        controller = require('./controller'),
        nodebb = require('./nodebb'),
        settings = require('./settings');
    var l24uConstants = require('./lk/constants');
    var l24uController = require('./lk/controller');
    var sockets = nodebb.pluginSockets;

    Sockets.init = function(callback) {
        sockets[constants.SOCKETS] = {};
        //Acknowledgements
        sockets[constants.SOCKETS].getCalculationProperties = Sockets.getCalculationProperties;
        sockets[constants.SOCKETS].getSettings = Sockets.getSettings;
        sockets[constants.SOCKETS].saveCalculationProperties = Sockets.saveCalculationProperties;
        sockets[constants.SOCKETS].saveSettings = Sockets.saveSettings;

        //l24u
        sockets[l24uConstants.SOCKETS] = {};
        sockets[l24uConstants.SOCKETS].saveAccount = Sockets.saveAccount;
        sockets[l24uConstants.SOCKETS].deleteAccount = Sockets.deleteAccount;
        sockets[l24uConstants.SOCKETS].getAccounts = Sockets.getAccounts;
        sockets[l24uConstants.SOCKETS].checkAccount = Sockets.checkAccount;
        callback();
    };

    Sockets.getCalculationProperties = function(socket, payload, callback) {
        controller.getCalculationProperties(callback);
    };

    Sockets.getSettings = function(socket, payload, callback) {
        controller.getSettings(callback);
    };

    Sockets.saveCalculationProperties = function(socket, payload, callback) {
        controller.saveCalculationProperties(payload, callback);
    };

    Sockets.saveSettings = function(socket, payload, callback) {
        controller.saveSettings(payload, callback);
    };

    //l24u
    Sockets.saveAccount = function(socket, payload, callback) {
        l24uController.saveAccount(payload, callback);
    };
    Sockets.deleteAccount = function(socket, payload, callback) {
        l24uController.deleteAccount(payload, callback);
    };
    Sockets.getAccounts = function(socket, payload, callback) {
        l24uController.getAccounts(payload, callback);
    };
    Sockets.checkAccount = function(socket, payload, callback) {
        l24uController.checkAccount(payload, callback);
    };

})(module.exports);