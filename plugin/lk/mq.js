var amqp = require('amqplib/callback_api');

var nconf        = require('../nodebb').nconf

var configs = nconf.get('mq');
var async = require('async');

module.exports = function (message, callback){
    async.waterfall([
        function(next) {
            var connectionUrl = `amqp://${configs.RABBITMQ_LOGIN}:${configs.RABBITMQ_PASS}@${configs.RABBITMQ_ADDRESS}`;
            amqp.connect(connectionUrl, function(err, conn) {
            conn.createChannel(function(err, ch) {
                    var ex = configs.RABBITMQ_QUEUE_LK;
                    var msg = message;
                    ch.assertExchange(ex, 'fanout', {durable: false});
                    ch.publish(ex, '', new Buffer(msg));
                    console.log(" [x] Sent %s", msg);
                    next(null);
                });
            });
        }
    ], callback);;
    
}