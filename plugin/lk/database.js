(function(Database) {
    'use strict';

    var async = require('async');

    var nodebb = require('../nodebb'),
        constants = require('./constants');

    var db = nodebb.db,
        user = nodebb.user;
        
    Database.getUserIds = function(done) {
        async.waterfall([
            async.apply(user.getUidsFromSet, 'users:joindate', 0, 1000000),
            function(uids, next) {
               next(null, uids);
            }
        ],  done);
    };

    Database.saveGameAccount = function(acc, done) {
        async.waterfall([
            function(next) {
                async.parallel([
                    async.apply(db.setAdd, constants.NAMESPACE + ':' + acc.uid, acc.login),
                    async.apply(db.setAdd, constants.NAMESPACE + ':logins', acc.login),
                    async.apply(db.setObject, constants.NAMESPACE + ':' + acc.uid + ':' + acc.login, acc),
                ], next)
            }
        ], done);
    };

    Database.deleteAccount = function(acc, done) {
        async.waterfall([
            function(next) {
                async.parallel([
                    async.apply(db.setRemove, constants.NAMESPACE + ':' + acc.uid, acc.login),
                    async.apply(db.delete, constants.NAMESPACE + ':' + acc.uid + ':' + acc.login),
                ], next)
            }
        ], done);
    };

    Database.checkAccount = function(acc, done) {
        async.waterfall([
            function(next) {
                async.parallel([
                    async.apply(db.isSetMember, constants.NAMESPACE + ':logins', acc.login)
                ], next)
            }
        ], done);
    };

    Database.getAccounts = function(uid, done) {
        async.waterfall([
            async.apply(db.getSetMembers, constants.NAMESPACE + ':' + uid),
            function(members, next) {
                let keys = members.map(function(mem){
                    return constants.NAMESPACE + ':' + uid + ':' + mem;
                });
                db.getObjects(keys, next);
            }
        ], done)
    };



})(module.exports);