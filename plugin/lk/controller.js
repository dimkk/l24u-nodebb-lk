 (function(Controller) {
     'use strict';

     var async = require('async'),
         nconf = require('../nodebb').nconf,
         objectAssign = require('object-assign');

     var database = require('./database');
     var validator = require('./validator');
     var mq = require('./mq');

     var user = require('./user');;
     Controller.saveAccount = function(payload, done) {

         async.waterfall([
             function(callback) {
                 database.getAccounts(payload.uid, callback);
             },
             function(accounts, callback) {
                 //console.log(accounts);
                 var result = validator(accounts, payload);
                 if (result.length === 0) {
                     async.parallel([
                         async.apply(database.saveGameAccount, payload),
                         function(cb) {
                             var msg = {
                                 section: "lk",
                                 method: "createAccount",
                                 hash: {
                                     "login": payload.login,
                                     "password": payload.password,
                                     "email": user().user.uid + ':' + user().user.username + ':' + user().user.email,
                                     "actor": user().user.username
                                 }
                             };
                             mq(JSON.stringify(msg));
                             cb();
                         }
                     ], callback)
                 } else {
                     callback(result);
                 }
             }
         ], done);
     }
     Controller.getAccounts = function(payload, done) {
         async.waterfall([
             function(callback) {
                 database.getAccounts(payload, callback);
             }
         ], done);
     }
     Controller.deleteAccount = function(payload, done) {
         //console.log(payload);
         async.waterfall([
             function(callback) {
                 database.deleteAccount(payload, callback);
             }
         ], done);
     }
     Controller.checkAccount = function(payload, done) {
         async.waterfall([
             function(callback) {
                 database.checkAccount(payload, callback);
             }
         ], done);
     }
 })(module.exports);