var database = require('./database');
module.exports = function(accounts, acc, callback){
    var result = '';
    result += validateLog(acc.login);
    result += validatePass(acc.password);
    result += accounts.length > 4 ? 'Нельзя создать больше 5 аккаунтов' : '';
    if (callback) callback(result);
    else return result;
}

function validateLog (login) {
            var result = '';
            var re = /^[A-Za-z0-9]*$/;
            var lengthTest = login.length > 4 && login.length < 15
            var reTest =  re.test(login);
            if (!lengthTest) {
                result += 'Длина должна быть больше 4 символов, и меньше 15 \n'
            }
            if (!reTest) {
                result += 'В логине должны быть только английские буквы и цифры \n';
            }
            return result;
        }
function validatePass (password) {
            var result = '';
            var re = /^[A-Za-z0-9]*$/;
            var lengthTest = password.length > 4 && password.length < 17
            var reTest =  re.test(password);
            if (!lengthTest) {
                result += 'Длина должна быть больше 4 символов, и меньше 17'
            }
            if (!reTest) {
                result += 'В пароле должны быть только английские буквы и цифры';
            }
            return result;
        }