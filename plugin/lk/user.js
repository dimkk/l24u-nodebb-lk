var mw      = require('../nodebb').middleware;
var u    = require('../nodebb').user;
var async = require('async');

var currentUser;
var isAdmin;

module.exports = function(req, next) {
    if (req) {
        var uid = req.uid;
        async.parallel([
            function(cb){
                u.getUsersFields([uid], ['email', 'username'], function (error, users) {
                    if (error) {
                        console.log(error);
                    }
                    currentUser = users[0];
                    cb();
                });
            },
            function(cb){
                u.isAdministrator(uid, function(result) {
                    isAdmin = result;
                    cb();
                })
            }
        ], next);
    } else {
        return {
            user: currentUser,
            isAdmin: isAdmin
        }
    }
}