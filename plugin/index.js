(function(Plugin) {
    'use strict';

    var async = require('async');

    var actions = require('./actions'),
        lkCtrl = require('./lk/controller'),
        controller = require('./controller'),
        files = require('./files'),
        filters = require('./filters'),
        settings = require('./lk/settings'),
        sockets = require('./sockets'),
        user = require('./lk/user');

    //NodeBB list of Hooks: https://github.com/NodeBB/NodeBB/wiki/Hooks
    Plugin.hooks = {
        actions: actions,
        filters: filters,
        statics: {
            load: function(params, callback) {
                var router = params.router,
                    middleware = params.middleware,
                    controllers = params.controllers,
                    pluginUri = '/admin/plugins/points',

                    renderAdmin = function(req, res, next) {
                        res.render(pluginUri.substring(1), {});
                    },

                    

                    renderOverviewSection = function(req, res, next) {
                        controller.getTopUsers(function(error, payload) {
                            if (error) {
                                return res.status(500).json(error);
                            }
                            res.render('client/points/overview', payload);
                        });
                    };

                router.get(pluginUri, middleware.admin.buildHeader, renderAdmin);
                router.get('/api' + pluginUri, renderAdmin);

                // Overview Page
                router.get('/user/:userslug/points', middleware.buildHeader, renderOverviewSection);
                router.get('/api/user/:userslug/points', renderOverviewSection);

                // L24u lk
                var renderLk = function(req, res, next) {
                    if (!req.uid) {
                        return res.status(401).json('not-authorized');
                    }
                    
                    res.render('client/l24u/lk', {});
                    user(req.user);
                }

                router.get('/lk', middleware.buildHeader, renderLk);
                router.get('/api/lk', renderLk);

                async.parallel({
                    settings: async.apply(settings.init),
                    sockets: async.apply(sockets.init),
                    files: async.apply(files.init)
                }, callback);
            }
        }
    };
})(module.exports);