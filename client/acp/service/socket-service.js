import * as SocketApi from '../model/socket-api';

export default class SocketService {
    static getCalculationProperties(done) {
        window.socket.emit(
            SocketApi.GET_CALCULATION_PROPERTIES,
            {},
            (error, properties) => {
                if (error) {
                    //App.alertError(error.message);
                }
                done(error, properties);
            }
        );
    }

    static getSettings(done) {
        window.socket.emit(
            SocketApi.GET_SETTINGS,
            {},
            (error, settings) => {
                if (error) {
                    //App.alertError(error.message);
                }
                done(error, settings);
            }
        );
    }

    static saveCalculationProperties(props, done) {
        window.socket.emit(
            SocketApi.SAVE_CALCULATION_PROPERTIES,
            props,
            (error) => {
                if (error) {
                    //App.alertError(error.message);
                }
                done(error);
            }
        );
    }

    static saveSettings(settings, done) {
        window.socket.emit(
            SocketApi.SAVE_SETTINGS,
            settings,
            (error) => {
                if (error) {
                    //App.alertError(error.message);
                }
                done(error);
            }
        );
    }

    static saveAccount(account, done) {
        window.socket.emit(
            SocketApi.SAVE_ACCOUNT,
            Object.assign({uid: window.app.user.uid}, account),
            (error, account) => {
                if (error) {
                    //App.alertError(error.message);
                    console.log(error);
                }
                done(error, account);
            }
        );
    }

    static deleteAccount(account, done) {
        window.socket.emit(
            SocketApi.DELETE_ACCOUNT,
            Object.assign({uid: window.app.user.uid}, account),
            (error, account) => {
                if (error) {
                    //App.alertError(error.message);
                    console.log(error);
                }
                done(error, account);
            }
        );
    }

    static getAccounts(done) {
        window.socket.emit(
            SocketApi.GET_ACCOUNTS,
            window.app.user.uid,
            (error, accounts) => {
                if (error) {
                    //App.alertError(error.message);
                    console.log(error);
                }
                done(error, accounts);
            }
        );
    }
    static getParams(done) {
        window.socket.emit(
            SocketApi.GET_PARAMS,
            window.app.user.uid,
            (error, params) => {
                if (error) {
                    //App.alertError(error.message);
                    console.log(error);
                }
                done(error, params);
            }
        );
    }
    static checkLogin(login, done) {
        window.socket.emit(
            SocketApi.GET_LOGIN_CHECK,
            {login: login, uid:window.app.user.uid},
            (error, check) => {
                if (error) {
                    //App.alertError(error.message);
                    console.log(error);
                }
                done(error, check);
            }
        );
    }
}
