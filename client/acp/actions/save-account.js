import { accountSaved } from './simple-actions';
import SocketService from '../service/socket-service';

export default function (account) {
    return (dispatch, getState) => {
        SocketService.saveAccount(account, error => {
            if (!error) {
                dispatch(accountSaved(account));
            }
        });
    };
}
