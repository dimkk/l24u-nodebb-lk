import { gotLoginCheck } from './simple-actions';
import SocketService from '../service/socket-service';

export default function (login) {
    return (dispatch, getState) => {
        SocketService.checkLogin(login, (error, check) => {
            if (!error) {
                //console.log(check);
                dispatch(gotLoginCheck(!check[0])); //Redis проверяет наличие - фолс если нет, нам надо другое.
            } else {
                console.log(error);
            }
        });
    };
}
