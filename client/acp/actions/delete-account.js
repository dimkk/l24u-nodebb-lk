import { accountDeleted } from './simple-actions';
import SocketService from '../service/socket-service';

export default function (account) {
    return (dispatch, getState) => {
        SocketService.deleteAccount(account, error => {
            if (!error) {
                dispatch(accountDeleted(account));
            }
        });
    };
}
