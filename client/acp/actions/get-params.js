import { gotParams } from './simple-actions';
import SocketService from '../service/socket-service';

export default function () {
    return (dispatch, getState) => {
        SocketService.getParams((error, params) => {
            if (!error) {
                dispatch(gotParams(params));
            }
        });
    };
}
