import { gotAccounts } from './simple-actions';
import SocketService from '../service/socket-service';

export default function () {
    return (dispatch, getState) => {
        SocketService.getAccounts((error, accounts) => {
            if (!error) {
                dispatch(gotAccounts(accounts));
            }
        });
    };
}
