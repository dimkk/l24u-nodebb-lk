import Dashboard from './view/widget/dashboard';
import React from 'react';
import ReactDom from 'react-dom';
require('./style/acp.scss');
export function init() {
    document.getElementsByTagName('title')[0].text = 'Личный кабинет ' + document.getElementsByTagName('title')[0].text;
    return ReactDom.render( 
        <Dashboard /> ,
        document.getElementById('lk-app')
    );
}