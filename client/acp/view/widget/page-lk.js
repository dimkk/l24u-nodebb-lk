import React from 'react';
import { connect } from 'react-redux';
import ActionButton from '../display/action-button';
import AccountsTable from '../display/accounts-table';
import AccountForm from '../display/account-form';
import saveAccount from '../../actions/save-account';
import deleteAccount from '../../actions/delete-account';
import checkLogin from '../../actions/check-login';
import { selectAccount, updateLkProperty } from '../../actions/simple-actions';

class PageLk extends React.Component {
    constructor(props) {
        super(props);
        this.saveAccount = () => {
            if (this.props.validation.loginValid && this.props.validation.passwordValid)
                this.props.dispatch(saveAccount(this.props.selectedAccount));
        };
        this.deleteAccount = (e, acc) => {
            e.stopPropagation();
            let ok = window.confirm('Вы собираетесь удалить игровой аккаунт, всех персонажей и вещи на нем, Вы уверены?');
            if (ok)
                this.props.dispatch(deleteAccount(acc));
        }
        this.selectAccount = (acc) => {
            if (this.props.selectedAccount.login === acc.login) {
                this.props.dispatch(selectAccount({login:'', password:''}));
            } else {
                this.props.dispatch(selectAccount(acc));
            }
        }
        this.checkLogin = () => {
            if (this.props.validation.loginValid)
                this.props.dispatch(checkLogin(this.props.selectedAccount.login));
        }
    }
    render() {
        return ( 
            <div className="row">
                <div className="col-xs-12">
                    <div>
                        <AccountsTable 
                            selectAccount={this.selectAccount}
                            selectedAccount={this.props.selectedAccount}
                            accounts={this.props.accounts}
                            deleteAccountHandler={this.deleteAccount}
                        />
                        <AccountForm 
                            loginVal={this.props.selectedAccount.login}
                            onLoginChange={(e) => this.propertyDidChange('login', e.target.value)}
                            loginValidationResult={this.props.validation.login}
                            passVal={this.props.selectedAccount.password}
                            onPassChange={(e) => this.propertyDidChange('password', e.target.value)}
                            passValidationResult={this.props.validation.password}
                            onBlurLoginHandler={this.checkLogin}
                        />
                    </div>
                    <ActionButton
                            clickHandler={this.saveAccount}
                            icon="fa fa-floppy-o" 
                            text="Сохранить игровой аккаунт"
                            enabled={!this.props.validation.saveDisabled}
                            clas="btn btn-primary"
                            />
                </div>
            </div>
        );
    }
    propertyDidChange(property, value) {
        this.props.dispatch(updateLkProperty(property, value));
    }
}



export default connect(state => {
    return {
        accounts       : state.accounts,
        selectedAccount: state.selectedAccount,
        validation     : state.validation
    };
})(PageLk);