import classNames from 'classnames';
import React from 'react';
import {connect} from 'react-redux';

import {changeSection} from '../../controller/actions';
import * as Pages from '../../model/pages';
import PageManage from './page-manage';
import PagePlugins from './page-plugins';
import PageRanking from './page-ranking';
import PageSettings from './page-settings';
import PageLk from './page-lk';

class TabHost extends React.Component {
    createSection(page) {
        switch (page) {
            case Pages.RANKING:
                return <PageRanking />;
            case Pages.PLUGINS:
                return <PagePlugins />;
            case Pages.MANAGE:
                return <PageManage />;
            case Pages.SETTINGS:
                return <PageSettings />;
            case Pages.LK:
                return <PageLk />;
            default:
                return null;
        }
    }

/*
<ul className="nav nav-tabs">
                {this.props.sections.map((section) => {
                    let liClass = classNames({active: this.props.section === section.value});
                    return (
                        <li className={liClass} onClick={(e) => this.props.dispatch(changeSection(section.value))}>
                            <a href="#" data-section={section.value}>{section.name}</a>
                        </li>
                    );
                })}
            </ul>

            <div className="tab-content">
                <div className="tab-pane active">
                    
                </div>
            </div>
 */

    render() {
        return <div className="tab-host">
            {this.createSection(this.props.section)}

            
        </div>
    }
}

export default connect((state) => {
    return {
        section : state.section,
        sections: state.sections
    };
})(TabHost);
