import React from 'react';

const AccountForm = ({
    loginVal, onLoginChange, loginValidationResult,
    passVal, onPassChange, passValidationResult,
    onBlurLoginHandler
    }) => {
    let getClassName = (field) => {
            let result = 'form-group';
            if (field === 'login') {
                if (loginValidationResult) {
                    result += ' has-error'
                }
            }
            if (field === 'password') {
                if (passValidationResult) {
                     result += ' has-error'
                } 
            }
            return result;
        }
        return (
            <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <div className={getClassName('login')}>
                            <label htmlFor="overviewMembers">Логин</label>
                            <input
                                className="form-control"
                                id="login"
                                value={loginVal}
                                onChange={onLoginChange}
                                onBlur={onBlurLoginHandler}
                                />
                             {loginValidationResult}
                        </div>
                        <div className={getClassName('password')}>
                            <label htmlFor="overviewMembers">Пароль</label>
                            <input
                                className="form-control"
                                id="password"
                                value={passVal}
                                onChange={onPassChange} 
                                />
                            {passValidationResult}
                        </div>
                </div>
            </div>
        );   
};

export default AccountForm;
