import React from 'react';

const ActiontButton = ({clickHandler, text, icon, clas, enabled}) => {
        return (
            <button
                disabled={!enabled}
                className={clas}
                type="button"
                onClick={clickHandler}><i className={icon}></i> {text}
            </button>
        );
};

export default ActiontButton;
