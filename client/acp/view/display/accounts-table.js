import React from 'react';

const AccountsTable = ({accounts, selectAccount, selectedAccount, deleteAccountHandler}) => {
    var getClass = (acc) => {
        if (acc.login === selectedAccount.login) {
            return 'active';
        } else {
            return '';
        }
    }
    if (accounts.length > 0) {
        return (
            <div className="table-responsive">
					<table className="table">
						<thead>
							<tr>
								<th>Логин игрового аккунта</th>
								<th>Пароль игрового аккаунта</th>
                                <th>Управление</th>
							</tr>
						</thead>
						<tbody>
                            {accounts.map(acc => {
                                return (
                                    <tr className={getClass(acc)} onClick={ () => selectAccount(acc)}>
                                        <td>{acc.login}</td>
                                        <td>{acc.password}</td>
                                        <td><i onClick={(e) => deleteAccountHandler(e, acc)} className="fa fa-minus-square pointer" aria-hidden="true"> Удалить</i></td>
                                    </tr>
                                );
                            })}
						</tbody>
					</table>
				</div>
        );
    } else {
        return (
            <h3>У Вас пока нет игровых аккаунтов!</h3>
        )
    }
        
};

export default AccountsTable;
