export const GET_CALCULATION_PROPERTIES = 'plugins.ns-points.getCalculationProperties';
export const GET_SETTINGS = 'plugins.ns-points.getSettings';
export const SAVE_CALCULATION_PROPERTIES = 'plugins.ns-points.saveCalculationProperties';
export const SAVE_SETTINGS = 'plugins.ns-points.saveSettings';

export const SAVE_ACCOUNT = 'plugins.l24u-lk.saveAccount';
export const DELETE_ACCOUNT = 'plugins.l24u-lk.deleteAccount';
export const GET_PARAMS = 'plugins.l24u-lk.getParams';
export const GET_ACCOUNTS = 'plugins.l24u-lk.getAccounts';
export const GET_LOGIN_CHECK = 'plugins.l24u-lk.checkAccount';