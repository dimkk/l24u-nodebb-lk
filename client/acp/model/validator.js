import ReduxStore from './redux-store';

export default class Validator {
    static state() { return ReduxStore.currentStore.store.getState() };
    static validateLkProps(obj) {
        let result = {};
        let newValidtion = Object.assign({}, this.state().validation);
        if (obj["login"]) {
            result.login = this.validateLog(obj["login"]);
            if (this.state().accounts.length > 4) {
                result.login += ' Увы, пока можно создать, только 5 игровых аккаунтов'
            }
        }
        if (obj["password"]) {
            result.password = this.validatePass(obj["password"]);
        }
        newValidtion = Object.assign(newValidtion, result);
        result.loginValid = newValidtion.login.length === 0 && this.state().selectedAccount.login.length > 0;
        result.passwordValid = newValidtion.password.length === 0 && this.state().selectedAccount.password.length > 0;
        result.saveDisabled = !result.loginValid || !result.passwordValid;
        result.delDisabled = !this.state().selectedAccount || (!obj["login"] || obj["login"] != this.state().selectedAccount.login);
        return result;
    }

    static validateLog (login) {
            let result = '';
            const re = /^[A-Za-z0-9]*$/;
            const lengthTest = login.length > 4 && login.length < 15
            const reTest =  re.test(login);
            if (!lengthTest) {
                result += 'Длина должна быть больше 4 символов, и меньше 15 \n'
            }
            if (!reTest) {
                result += 'В логине должны быть только английские буквы и цифры \n';
            }
            return result;
        }
    static validatePass (password) {
            let result = '';
            const re = /^[A-Za-z0-9]*$/;
            const lengthTest = password.length > 4 && password.length < 17
            const reTest =  re.test(password);
            if (!lengthTest) {
                result += 'Длина должна быть больше 4 символов, и меньше 17'
            }
            if (!reTest) {
                result += 'В пароле должны быть только английские буквы и цифры';
            }
            return result;
        }
}