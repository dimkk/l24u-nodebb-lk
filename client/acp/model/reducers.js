import u from 'updeep';

import * as ActionTypes from './action-types';
import * as Pages from './pages';
import Validator from './validator';

const PAGES = [
    {name: 'L24u-LK', value: Pages.LK},
    {name: 'Ranking', value: Pages.RANKING},
    {name: 'Manage', value: Pages.MANAGE},
    {name: 'Integrations', value: Pages.PLUGINS},
    {name: 'Settings', value: Pages.SETTINGS}
    
];

export function calculationProperties(state = {}, action) {
    switch (action.type) {
        case ActionTypes.CALCULATION_PROPERTY_WILL_UPDATE:
            let propertyUpdate = {};
            propertyUpdate[action.payload.property] = action.payload.value;
            return u(propertyUpdate, state);
        case ActionTypes.CALCULATION_PROPERTIES_DID_UPDATE:
            return u(action.payload, state);
        default:
            return state;
    }
}

export function calculationPropertiesChanged(state = false, action) {
    switch (action.type) {
        case ActionTypes.CALCULATION_PROPERTY_WILL_UPDATE:
            return true;
        case ActionTypes.CALCULATION_PROPERTIES_DID_STORE:
            return false;
        default:
            return state;
    }
}

export function section(state = Pages.LK, action) {
    switch (action.type) {
        case ActionTypes.SECTION_WILL_CHANGE:
            return action.payload.section;
        default:
            return state;
    }
}

export function sections(state = PAGES, action) {
    return state;
}

export function settings(state = {}, action) {
    switch (action.type) {
        case ActionTypes.OVERVIEW_MAX_USERS_WILL_CHANGE:
            return u({maxUsers: action.payload}, state);
        case ActionTypes.SETTINGS_DID_UPDATE:
            return u(action.payload, state);
        default:
            return state;
    }
}

export function settingsChanged(state = false, action) {
    switch (action.type) {
        case ActionTypes.OVERVIEW_MAX_USERS_WILL_CHANGE:
            return true;
        case ActionTypes.SETTINGS_DID_STORE:
            return false;
        default:
            return state;
    }
}

export function accounts(state = [], action) {
    switch (action.type) {
        case ActionTypes.ACCOUNT_DID_SAVE:
            var acc = action.payload;
            if (state.filter((el) => el.login === acc.login).length > 0) {
                const index = state.findIndex(el => el.login === acc.login);
                state.splice(index, 1);
                state.push(acc);
            } else {
                state.push(acc);
            }
            return state.slice();
        case ActionTypes.ACCOUNT_DID_DELETE:
            var acc = action.payload;
            if (state.filter((el) => el.login === acc.login).length > 0) {
                const index = state.findIndex(el => el.login === acc.login);
                state.splice(index, 1);
            }
            return state.slice();
        case ActionTypes.GOT_ACCOUNTS:
            return action.payload;
        default:
            return state;
    }
}
export function selectedAccount(state = {login: '', password: ''}, action) {
    //console.log(action);
    switch (action.type) {
        case ActionTypes.ACCOUNT_DID_SAVE:
            return u({login: '', password: ''}, state);
        case ActionTypes.ACCOUNT_SELECT:
            return u(action.payload, state);
        case ActionTypes.LK_PROPERTY_UPDATE:
            let propertyUpdate = {};
            propertyUpdate[action.payload.property] = action.payload.value;
            return u(propertyUpdate, state);
        case ActionTypes.ACCOUNT_DID_DELETE:
            var acc = action.payload;
            if (state.login === acc.login) {
                return u({login: '', password: ''}, state);
            } else {
                return state;
            }
            
        default:
            return state;
    }
}

export function validation(state = {login: '', password: '', delDisabled: true, saveDisabled: true}, action) {
    switch (action.type) {
        case ActionTypes.GOT_LOGIN_CHECK:
            let result = '';
            if (!action.payload) {
                result = ' Логин уже занят! Выбери другой! '
                return u({login: result, loginValid: false, saveDisabled: true}, state);
            } else {
                return state;
            }
        case ActionTypes.ACCOUNT_SELECT:
            return u({delDisabled: false}, state);
        case ActionTypes.LK_PROPERTY_UPDATE:
            let propertyUpdate = {};
            propertyUpdate[action.payload.property] = action.payload.value;
            const validationResult = Validator.validateLkProps(propertyUpdate);
            //console.log(validationResult);
            return u(validationResult, state);
        default:
            return state;
    }
}