var path = require('path'),
    webpack = require('webpack');

module.exports = {
    entry: "./index.js",
    output: {
        path: "../../public/js",
        filename: "lk.js",
        libraryTarget: "amd",
        library: "forum/client/l24u/lk"
    },
    module: {
        loaders: [{
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    watchOptions: {
        poll: 1000
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ]
};